package com.itreflection.project15.learning.task2.impl;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.itreflection.project15.learning.task2.api.model.ValidationDto;

@RunWith(MockitoJUnitRunner.class)
public class EmailValidatorImplTest {

  @InjectMocks
  private EmailValidatorImpl emailValidator;

  @Test
  public void shouldHaveNoErrorsWhenValidEmail() throws Exception {
    //given
    String email = "testTest@email.emaa.com";

    //when
    ValidationDto result = emailValidator.validate(email);

    //then
    assertThat(result).isNotNull();
    assertThat(result.getErrors()).isEmpty();
  }


}