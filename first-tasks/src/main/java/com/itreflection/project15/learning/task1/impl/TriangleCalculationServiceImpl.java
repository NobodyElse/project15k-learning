package com.itreflection.project15.learning.task1.impl;

        import com.itreflection.project15.learning.task1.api.TriangleCalculationService;
        import com.itreflection.project15.learning.task1.api.TriangleParameterException;

        import java.util.Arrays;

public class TriangleCalculationServiceImpl implements TriangleCalculationService {

    private void validate (int param, String msg) {
        if (param <= 0) {
            throw new TriangleParameterException(String.format(msg, param));
        }
    }
    private void validateIfTwoParamsAreGreaterThanThird (int param1, int param2, int param3, String msg){
        int[] tab = {param1, param2, param3};
        Arrays.sort(tab);
        if (param1 + param2 <= param3) {
            throw new TriangleParameterException(String.format(msg, param1, param2, param3));
        }
    }
    private double calculate (int param1, int param2, int param3) {
        return param1+param2+param3;
    }
    @Override
    public double calculatePerimeter(int a, int b, int c) {
        validate(a, "a <= 0");
        validate(b, "b <= 0");
        validate(c, "c <= 0");
        validateIfTwoParamsAreGreaterThanThird (a, b, c, "Sum of shorter sides should be greater than longest side. "+a+", "+b+", "+c+" You can't create triangle with those values.");
        return calculate(a, b, c);
    }
}

