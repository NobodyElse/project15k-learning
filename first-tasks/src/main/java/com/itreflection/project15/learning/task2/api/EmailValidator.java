package com.itreflection.project15.learning.task2.api;

import com.itreflection.project15.learning.task2.api.model.ValidationDto;

public interface EmailValidator {

  ValidationDto validate(String email);
}
