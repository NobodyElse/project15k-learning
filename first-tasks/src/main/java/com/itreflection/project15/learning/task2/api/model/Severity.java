package com.itreflection.project15.learning.task2.api.model;

public enum Severity {
  INFO,
  WARN,
  ERROR,
}
