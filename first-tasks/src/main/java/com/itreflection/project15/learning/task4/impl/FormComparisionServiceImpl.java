package com.itreflection.project15.learning.task4.impl;

import com.itreflection.project15.learning.task4.api.model.Form;
import com.itreflection.project15.learning.task4.api.service.FormComparisionService;

public class FormComparisionServiceImpl implements FormComparisionService {

   @Override
   public int comparePerimeter(Form form1, Form form2) {
       return form1.calculateField().compareTo(form2.calculateField());
   }

   @Override
   public int compareField(Form form1, Form form2) {
      return form1.calculatePerimeter().compareTo(form2.calculatePerimeter());
   }
}
