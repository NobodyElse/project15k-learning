package com.itreflection.project15.learning.task2.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.itreflection.project15.learning.task2.api.EmailValidator;
import com.itreflection.project15.learning.task2.api.model.Error;
import com.itreflection.project15.learning.task2.api.model.Severity;
import com.itreflection.project15.learning.task2.api.model.ValidationDto;

public class EmailValidatorImpl implements EmailValidator {


  private Pattern pattern;
  private Matcher matcher;
  private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]{0,10}@[A-Za-z0-9-]{0,8}+(\\.[A-Za-z0-9]{2,4})*\\.[A-Za-z]{2,}$";


  private ValidationDto regexValidation(String email) {
    ValidationDto newVal = new ValidationDto();
    Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    Matcher matcher = pattern.matcher(email);
    if (!matcher.matches()) {
      newVal.add(new Error(12L, "msg", Severity.INFO));
    }
    return newVal;
  }
  @Override
  public ValidationDto validate (String email) {
    return regexValidation(email);

  }
}
