package com.itreflection.project15.learning.task2.api.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class Error {

  private long code;
  private String msg;
  private Severity severity;

  public Error(long code, String msg, Severity severity) {
    this.code = code;
    this.msg = msg;
    this.severity = severity;
  }

  public long getCode() {
    return code;
  }

  public void setCode(long code) {
    this.code = code;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Severity getSeverity() {
    return severity;
  }

  public void setSeverity(Severity severity) {
    this.severity = severity;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("code", code).append("msg", msg)
        .append("severity", severity).toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    Error rhs = (Error) obj;
    return new EqualsBuilder().append(this.code, rhs.code).append(this.msg, rhs.msg).append(this.severity, rhs.severity)
        .isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(code).append(msg).append(severity).toHashCode();
  }
}