package com.itreflection.project15.learning.task3.impl;

import java.util.List;

import com.itreflection.project15.learning.task3.api.BadWordsFilter;

public class BadWordsFilterImpl implements BadWordsFilter {

  private List<String> wordsFilter(List<String> words)
  {
    for (String e : words)
    {
      if (e.toLowerCase().equals("shit") || e.toLowerCase().equals("fuck"))
        words.remove(e);
    }
    return words;

  }
  @Override
  public List<String> filter(List<String> words) {
    return wordsFilter(words);
  }
}
