package com.itreflection.project15.learning.task4.api.model;

public interface Form {

   Double calculateField();

   Double calculatePerimeter();
}
