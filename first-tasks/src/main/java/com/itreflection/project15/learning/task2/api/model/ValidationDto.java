package com.itreflection.project15.learning.task2.api.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ValidationDto {

  private List<Error> errors;

  public ValidationDto() {
    this.errors = new ArrayList<>();
  }

  public ValidationDto(List<Error> errors) {
    this.errors = errors;

    if (errors == null) {
      this.errors = new ArrayList<>();
    }

  }

  public void add(Error error) {
    errors.add(error);
  }

  public List<Error> getErrors() {
    return errors;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("errors", errors).toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    ValidationDto rhs = (ValidationDto) obj;
    return new EqualsBuilder().append(this.errors, rhs.errors).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(errors).toHashCode();
  }
}
